import unittest
from solution import solution


class TestSolution(unittest.TestCase):

    # test first example provided in documentation of problem
    def test_first_documented_example(self):
        self.assertEqual(solution(A=[6, 1, 4, 6, 3, 2, 7, 4], K=3, L=2), 24,
                         "Should be 24")

    # test second example provided in documentation of problem
    def test_fail_when_disjoint_seqments_not_possible(self):
        self.assertEqual(solution(A=[10, 19, 15], K=2, L=2), -1,
                         "Should be -1")

    # test to ensure that no overlap of sub-strings is occurring
    def test_no_double_counting_across_alice_and_bob_when_K_before_L(self):
        self.assertEqual(solution(A=[300, 200, 100, 50, 25], K=3, L=2), 675,
                         "Should be 675")

    # test to ensure that no overlap of sub-strings is occurring
    def test_no_double_counting_across_alice_and_bob_when_L_before_K(self):
        self.assertEqual(solution(A=[80, 100, 5, 500, 500, 25, 14], K=2, L=1),
                         1100,
                         "Should be 1100")

    def test_minimum_number_of_trees(self):
        self.assertEqual(solution(A=[8, 8], K=1, L=1),
                         16,
                         "Should be 16")

    def test_alice_picksN_minus_1_trees(self):
        self.assertEqual(solution(A=[100, 100, 100, 100, 5], K=4, L=1),
                         405,
                         "Should be 405")

    def test_A_empty_returns_minus1(self):
        self.assertEqual(solution(A=[], K=3, L=2), -1,
                         "Should be -1")

    # ---------Assumption tests-------------------
    # problem definition says
    # "...given an array A consisting of N integers...Assume
    # N is an integer within the range [2..100];"
    # therefore should test what happens should A be below of this range
    def test_A_shorter_than_2_returns_minus1(self):
        self.assertEqual(solution(A=[1], K=3, L=2), -1,
                         "Should be -1")

    # problem definition says
    # "...given an array A consisting of N integers...Assume
    # N is an integer within the range [2..100];"
    # therefore should test what happens should A be above of this range
    def test_A_longer_than_100_elements_returns_minus1(self):
        a_with_101_elements = [1 for i in range(101)]
        self.assertEqual(solution(A=a_with_101_elements, K=2, L=2), -1,
                         "Should be -1")

    # problem definition says "Assume that:
    # N is an integer within the range [2..100];
    # K and L are integers in the range [1..N-1];
    def test_K_too_high_out_of_range(self):
        a_with_150_elements = [1 for i in range(150)]
        print(f'{a_with_150_elements}')
        self.assertEqual(solution(A=a_with_150_elements, K=110, L=1), -1,
                         "Should be -1")

    def test_K_too_low_out_of_range(self):
        self.assertEqual(solution(A=[3, 3, 3, 3, 3], K=-1, L=1), -1,
                         "Should be -1")

    # each element of array A is an integer in the range [1..500]
    def test_not_accepting_A_element_greater_than_500(self):
        self.assertEqual(solution(A=[1, 25, 100, 550, 60], K=3, L=2), -1,
                         "Should be -1")

    def test_if_K_L_not_int(self):
        self.assertEqual(solution(A=[1, 25, 100, 550, 60],
                         K='frog', L='burger'), -1,
                         "Should be -1")

    def test_A_Contains_non_ints(self):
        self.assertEqual(solution(A=[1, 25, 100, 'burger', 60, 5],
                         K=2, L=3), -1,
                         "Should be -1")


if __name__ == '__main__':
    unittest.main()
