# README #

Python solution to the problem of finding the 
maximum sum of elements in two non overlapping contiguous sub arrays

### Problem ###

Margot and Eliot must collect apples from a beautiful Fillorian orchard. ( A questing beast is involved - don't ask) 
There are N apples in the orchard. The apple trees are arranged in a row and they are numbered from 1 to N

Margot (aka Janet if you prefer the books) is planning to collect all the apples from K consecutive trees and Eliot
is planning to collect all the apples from L consecutive trees. They want to
choose two disjoint segments (one consisting of K trees for Margot and the
other consisting of L trees for Eliot) so as not to disturb each other. What is
the maximum number of apples that they can collect?

Write  a function:
`def solution(A, K, L)`
that, given an array A consisting of N integers denoting the number of apples
on each apple tree in the row, and integers K and L denoting, respectively,
the number of trees that Margot and Eliot can choose when collecting, returns
the maximum number of apples that can be collected by them, or -1 if there 
are no such intervals.

For example, given A=[6,1,4,6,3,2,7,4], K=3, L=2, your function should return
24, because Margot can choose 3 to 5 and collect 4+6+3=13 apples, and Eliot can
choose 7 to 8 and collect 7+4=11 apples. This, they will collect 13+11=24
apples in total and that is the maximum number that can be achieved.

Given A=[10,19,15], K=2,L=2, your function should return -1,
because it is not possible for Margot and Eliot to choose two disjoint intervals.

Assume that:
* N is an integer within the range [2..100];
* K and L are integers in the range [1..N-1];
* each element of array A is an integer in the range [1..500]

In your solution focus on correctness. The performance will not be the focus
of the assessment

### Setting up on your local machine ###
clone the repo
```bash
cd <location of clones repo>`
pip install pipenv
pipenv install
pipenv shell
```

### Check file formatting ###
```bash
sudo apt install pycodestyle
pycodestyle
``` 

### Run tests ###
```bash
python tests.py`
```

