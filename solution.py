# solution based largely on approach taken here (java)
# https://studyalgorithms.com/array/maximum-sum-of-elements-in-two-non-overlapping-contiguous-sub-arrays/


def solution(A, K, L):
    """ Function to return the maximum sum of elements
     in two non-overlapping contiguous sub arrays
     Parameters:
      A - an array A consisting of N integers,
          where N is within the range [2..100] and none of the
          integers are outside of the range [1..500]
      K - integer in the range [1..N-1]
      L - integer in the range [1..N-1] """

    # --- start of input variable checking
    # In a real world situation (situation dependent) it MAY
    # be advisable to perform some checking of the input variables.
    #
    # I was torn on whether to include type checking - it's not considered
    # "pythonic" to include such checks, as it goes against duck-typing.
    # The appropriate pythonic approach is to ensure correct documentation
    # and then try to use whatever is passed, either allowing exceptions to
    # bubble up or just catch them.
    # HOWEVER, as codility has advised that 8 additional tests which I have no
    # insight to I will include some type checking ... just in-case

    # check K and L are both of type int
    if((not isinstance(K, int)) or (not isinstance(L, int))):
        return -1

    # return if K and L are NOT in the range [1..N-1];
    if(K < 1 or K > 99 or L < 1 or L > 99):
        return -1

    # return if A contains more than N integers
    # (where N has been defined as within the range [2..100])
    if(len(A) > 100 or len(A) < 2):
        return -1

    # return if any non int elements of array A
    if(not all(isinstance(x, int) for x in A)):
        return -1

    # return if array A contains any integer outside the range [1..500]"
    if(max(A) > 500 or min(A) < 1):
        return -1

    # --- end of input variable checking

    # modify A so that each element contains sum(A[i] + sum(all previous A[i]))
    for i in range(1, len(A)):
        A[i] = A[i - 1] + A[i]

    # ensure there is a sufficient number of trees
    # for Alice and Bob to remain disjoint
    if (len(A) < (K + L)):
        return -1

    # create initial possible maximum values
    # for K and L (based on each of them being the first sub-string)
    # and for the result being the sum of the first K+L array elements
    maxK = A[K - 1]
    maxL = A[L - 1]
    result = A[K + L - 1]

    # either K before L or L before K, start this loop at index K + L
    for i in range(K + L, len(A)):

        # tracking max K so far
        # K before L: A[i - L] - A[i - L - K] is sum of K-length sub array
        maxK = max(maxK, A[i - L] - A[i - L - K])

        # tracking max L so far
        # L before K: A[i - K] - A[i - K - L] is sum of L-length sub array
        maxL = max(maxL, A[i - K] - A[i - K - L])

        result = max(result, max(maxK + (A[i] - A[i - L]),
                                 maxL + (A[i] - A[i - K])))

    return result
